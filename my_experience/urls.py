from django.conf.urls import url
from django.urls import path 
from my_experience.views import index

urlpatterns = [
    path('', index, name="index")
]